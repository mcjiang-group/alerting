package com.mcjiang.alerting.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.mcjiang.alerting.object.Alert;
import com.mcjiang.alerting.utils.DateUtils;
import com.mcjiang.alerting.utils.ResourceHelper;

public class FileProcessor {
	private final static int SATELLITEID_COL = 1;
	private final static int RED_HIGH_COL = 2;
	private final static int RED_LOW_COL = 5;
	private final static int COMP_COL = 7;
	private final static int TIMESTAMP_COL = 0;
	private final static String COMP_BATT = "BATT";
	private final static String COMP_TSTAT = "TSTAT";
	
	public static void main(String[] args) throws IOException {
		
       String fileName = ResourceHelper.getAbsoluteFilePath("data.txt");
       FileProcessor proc = new FileProcessor();
       proc.parseFile(fileName);

    }
	
	public void parseFile(String fileName) throws IOException {
		
		try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
		    Predicate<String> lowHighFilter = line -> {
			    String[] arr = line.split("\\|");
			    if (arr[COMP_COL].equals(COMP_BATT) && Integer.valueOf(arr[RED_LOW_COL]) < 10)
				    return true;
			    else if (arr[COMP_COL].equals(COMP_TSTAT) && Integer.valueOf(arr[RED_HIGH_COL]) > 100)
				    return true;
			    return false;
		    };
		  
		    Map<String, List<Alert>> alerts = lines		  
			    .filter(lowHighFilter)	  
		        .map(line -> {
			        String[] arr = line.split("\\|");
		            String severity = arr[COMP_COL].equals(COMP_BATT) ? "RED LOW" : "RED HIGH";
		            return new Alert(
		                Integer.valueOf(arr[SATELLITEID_COL]), 
		                severity, 
		                arr[COMP_COL],
		                DateUtils.toLocalDateTime(arr[TIMESTAMP_COL]));
		        }).collect(	Collectors.groupingBy(a -> a.getComponent()+a.getSatelliteId(),
		    		Collectors.toList()));

		    List<Alert> list = new ArrayList<Alert>();
		    alerts.keySet().forEach(key -> { 
			    List<Alert> lst = timeDuration(alerts.get(key));
			    if (lst.size() > 0)
				    list.addAll(lst);
		    });

            Gson gson = DateUtils.createGson();
            String jsonInString =  gson.toJson(list);
            System.out.println(jsonInString);
		}
		
    }
	
	private List<Alert> timeDuration(List<Alert> alerts) {
		List<Alert> retList = new ArrayList<Alert>();
		Alert alert = alerts.get(0);
		int cnt = 0;
		for (int i = 0; i< alerts.size(); i++) {
			Duration duration = Duration.between(alerts.get(i).getTimestamp(), alert.getTimestamp());
			if (duration.toMinutes() < 5) {
				cnt++;
			}
			if (cnt == 3) {
				retList.add(alert);
			}
		}
		return retList;
	}

	
}
