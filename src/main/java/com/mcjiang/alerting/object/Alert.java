package com.mcjiang.alerting.object;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.mcjiang.alerting.utils.DateUtils;

public class Alert implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//@JsonProperty("satelliteId")
	private int satelliteId;
	//@JsonProperty("severity")
    private String severity;
	//@JsonProperty("component")
    private String component;
//	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime timestamp;
    
    public Alert(int satelliteId, String severity, String component, LocalDateTime timestamp) {
    	this.satelliteId = satelliteId;
    	this.severity = severity;
    	this.component = component;
        this.timestamp = timestamp;
    }
    
	public int getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(int satelliteId) {
		this.satelliteId = satelliteId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
 
	@Override
    public String toString() {
		 
        return "{" + System.lineSeparator() +
                "\"satelliteId\": " + satelliteId + System.lineSeparator() +
                "\"severity\": " + severity + System.lineSeparator() +
                "\"component\": " + component + System.lineSeparator() +
                "\"timestamp\": " + DateUtils.toString(timestamp) + System.lineSeparator() +
                "}";
    }
    

}
