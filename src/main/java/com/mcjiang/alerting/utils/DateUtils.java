package com.mcjiang.alerting.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DateUtils {
	
	public static LocalDateTime toLocalDateTime(String dtStr) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        LocalDateTime parsedDateTime = LocalDateTime.parse(dtStr, formatter);
		return parsedDateTime;
				
	}

	public static String toString(LocalDateTime localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		String localDateString = localDateTime.format(formatter);
		return localDateString;
				
	}
	
	public static Gson createGson() {
		GsonBuilder builder = new GsonBuilder();
		builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		builder.setPrettyPrinting();
		builder.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter());
		Gson gson = builder.create();
		return gson;
	}
}
